> Quisiera quedarme aquí en mi casa
> pero ya no sé cual es

— Sur o no sur, Kevin Johansen


Quem é que tinha a perfeita consciência de si mesmo, da solidão absoluta que significa ter de entrar num cinema ou num bordel, ou em casa de amigos ou numa profissão absorvente ou no matrimônio para estar pelo menos só entre os demais.

— Jogo da amarelinha, Julio Cortázar


![america invertida](https://norte-oliviamaia.netlify.com/insp_/joaquin-torres-garcia-america-invertida-1943.jpg)

— America invertida, Joaquim Torres Garcia
